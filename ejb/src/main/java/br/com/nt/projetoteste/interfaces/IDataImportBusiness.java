package br.com.nt.projetoteste.interfaces;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;

@Local
public interface IDataImportBusiness extends Serializable {

	public static final String separator 			= File.separator;
	public static final String HOMEPATH 			= "HOMEPATH";
	public static final String READ_ARQUIVES_DIR 	= "data".concat(separator).concat("in");
	public static final String CREATE_ARQUIVE_DIR 	= "data".concat(separator).concat("out");

	List<String> paths = new ArrayList<String>();

	/**
	 * Metodo que le os arquivos do diretorio de entrada.
	 * @param fileName
	 * @param archiveName
	 * @throws IOException
	 */
	void loadFile(String fileName, String archiveName) throws IOException;

	/**
	 * Metodo que grava os dados no arquivo do diretorio de saida.
	 * @param lines
	 * @param fileName
	 * @throws IOException
	 */
	void sumarizarInformacoes(List<String> lines, String fileName) throws IOException;

	/**
	 * Metodo que conta o total de clientes dentro do arquivo.
	 * @param lines
	 * @return int
	 */
	int totalClientesDentroArquivo(List<String> lines);

	/**
	 * Metodo que conta o total de vendedores dentro do arquivo.
	 * @param lines
	 * @return int
	 */
	int totalVendedoresDentroArquivo(List<String> lines);

	/**
	 * Metodo que procura o id do vendedor que fez a venda mais alta. 
	 * @param lines
	 * @return int
	 */
	int getIdVendaValorMaisAlto(List<String> lines);

	/**
	 * Metodo que seleciona as vendas.
	 * @param string
	 * @return String
	 */
	String selecionaVendas(String string);

	/**
	 * Metodo que Seleciona o vendedor.
	 * @param string
	 * @return String
	 */
	String selecionaVendedor(String string);

	/**
	 * Metodo que seleciona o id do maior valor das vendas.
	 * @param vendas
	 * @return double
	 */
	double selecionaIdMaiorValor(List<String> vendas);

	/**
	 * Metodo que retorna array com os caminhos.
	 * @param path
	 * @return String[]
	 */
	String[] getFiles(String path);

	/**
	 * Metodo que verifica o tipo de arquivo.
	 * @param files
	 * @return boolean
	 */
	boolean arquiveTypeVerify(String[] files);

	/**
	 * Metodo que verifica se os arquivos existem.
	 * @param files
	 * @return boolean
	 */
	boolean arquiveExists(String[] files);


}