package br.com.nt.projetoteste.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Named;

import org.slf4j.Logger;

import br.com.nt.projetoteste.interfaces.IDataImportBusiness;
import br.com.nt.projetoteste.util.WarningType;

@Named
@Stateless
public class DataImportBusiness implements IDataImportBusiness {

	/**
	 * 
	 */
	private static final long serialVersionUID 		= 1L;

	private Logger log;
	public static final String READ_ARQUIVES_DIR 	= System.getenv(HOMEPATH).concat(separator).concat(IDataImportBusiness.READ_ARQUIVES_DIR);
	public static final String CREATE_ARQUIVE_DIR 	= System.getenv(HOMEPATH).concat(separator).concat(IDataImportBusiness.CREATE_ARQUIVE_DIR);

	public static final String FILE_NOT_FOUND = "directory is empty.";

	public int 					countFiles;

	public File 				fileIn;

	public List<String> 		paths = new ArrayList<String>();

	public String 				vendedorMenorValor;

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#loadFile(String, String)
	 */
	@Override
	public void loadFile(String fileName, String archiveName) throws IOException{

		InputStream is = new FileInputStream(fileName);
		InputStreamReader isr = new InputStreamReader(is);
		@SuppressWarnings(WarningType.RESOURCE)
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();

		List<String> lines = new ArrayList<String>();

		while (s != null) {
			log.info(s);
			s = br.readLine();
			lines.add(s);
		}

		lines.remove(null);

		sumarizarInformacoes(lines, archiveName);
		totalClientes = 0;
		totalVendedores = 0;

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#sumarizarInformacoes(List, String)
	 */
	@Override
	public void sumarizarInformacoes(List<String> lines, String fileName) throws IOException{

		log.info(fileName);
		FileOutputStream os = new FileOutputStream(CREATE_ARQUIVE_DIR.concat(separator).concat(fileName).concat(".done.dat"));
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter bw = new BufferedWriter(osw);

		//Quantidade de Clientes dentro do Arquivo OK
		bw.write("Quantidade de clientes no arquivo de entrada: "+totalClientesDentroArquivo(lines));
		bw.newLine();

		//Quantidade de Vendedores dentro do Arquivo
		bw.write("Quantidade de vendedor no arquivo de entrada: "+totalVendedoresDentroArquivo(lines));
		bw.newLine();

		//ID da Venda de valor mais alto
		bw.write("id da venda mais cara: "+getIdVendaValorMaisAlto(lines));
		bw.newLine();
		//Nome do Vendedor que menos vendeu
		bw.write("O pior vendedor: "+vendedorMenorValor);
		bw.newLine();

		bw.close();

	}

	int totalClientes = 0;
	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#totalClientesDentroArquivo(List)
	 */
	@Override
	public int totalClientesDentroArquivo(List<String> lines){


		lines.stream()
		  .filter(s -> s.startsWith("002"))
		  .forEach(s -> totalClientes++);
//		for (String string : lines) {
//			if (string.startsWith("002")) {
//				totalClientes++;
//			}
//		}
		return totalClientes;

	}

	int totalVendedores = 0;

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#totalVendedoresDentroArquivo(List)
	 */
	@Override
	public int totalVendedoresDentroArquivo(List<String> lines){


		lines.stream()
		  .filter(s -> s.startsWith("001"))
		  .forEach(s -> totalVendedores++);

//		for (String string : lines) {
//			if (string.startsWith("001")) {
//				totalVendedores++;
//			}
//		}
		return totalVendedores;

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#getIdVendaValorMaisAlto(List)
	 */
	@Override
	public int getIdVendaValorMaisAlto(List<String> lines){

		List<String> vendas = new ArrayList<String>();
		Map<Integer, Double> map = new HashMap<Integer, Double>();
		Map<Integer, String> map2 = new HashMap<Integer, String>();

		for (String string : lines) {

			if (string.startsWith("003")) {

				String s = selecionaVendas(string);
				log.info(s);
				vendas.add(s);
				map.put(Integer.parseInt(string.substring(4,6)), selecionaIdMaiorValor(vendas));
				map2.put(Integer.parseInt(string.substring(4,6)), selecionaVendedor(string));

			}

		}

		List<Double> idMaior = new ArrayList<Double>();

		double max = 0.0;

		for (Map.Entry<Integer, Double> entry : map.entrySet()){

			log.info("id: "+entry.getKey()+" valor: "+entry.getValue());
	        idMaior.add(entry.getValue());
	        max = Collections.max(idMaior);

		}

		List<Integer> idMenor = new ArrayList<Integer>();

		for (Map.Entry<Integer, String> entry : map2.entrySet()){
			idMenor.add(entry.getKey());
		}

		log.info(Collections.min(idMenor).toString());

		vendedorMenorValor = map2.get(Collections.min(idMenor));

		for (Map.Entry<Integer, Double> entry : map.entrySet()){
			if (entry.getValue() == max) {
				return entry.getKey();
			}
		}

		return 0;

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#selecionaVendas(String)
	 */
	@Override
	public String selecionaVendas(String string){

		log.info(string);
		StringBuffer buffer = new StringBuffer();

		for (int i = 0; i < string.length(); i++) {

			char c = string.charAt(i);

			if (Character.isDigit(c) || Character.isLetter(c) || (""+c).equals("[") || (""+c).equals("]") || (""+c).equals(".") || (""+c).equals(",") || (""+c).equals("-")) {

				log.info(c+" "+true);
				buffer.append(c);

			}else {

				log.info(c+" "+false);
				buffer.append(",");

			}

		}

		int indice_abre= buffer.toString().indexOf('[');  

		int indice_fecha= buffer.toString().lastIndexOf(']');

		String vendas= buffer.toString().substring(indice_abre + 1, indice_fecha);  

		log.info(vendas);//item-qtde-preco

		return vendas;

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#selecionaVendedor(String)
	 */
	@Override
	public String selecionaVendedor(String string){

		List<String> str = new ArrayList<String>();

		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			str.add(""+c);
		}

		Collections.reverse(str);

		log.info(""+str.indexOf("ç"));

		List<String> nome = new ArrayList<String>();

		int index = str.indexOf("ç");

		for (int i = 0; i < str.size(); i++) {

			if (i < index) {
				nome.add(str.get(i));
			}else{
				break;
			}

		}

		Collections.reverse(nome);

		StringBuffer buffer = new StringBuffer();

		for (int i = 0; i < nome.size(); i++) {
			buffer.append(nome.get(i));
		}

		return buffer.toString();

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#selecionaIdMaiorValor(List)
	 */
	@Override
	public double selecionaIdMaiorValor(List<String> vendas){

		String[] split = new String[vendas.size()];

		Map<String, Double> map = new HashMap<String, Double>();

		List<Double> valores = null;

		for (String string : vendas) {

			valores = new ArrayList<Double>();

			log.info(map.toString());

			split = string.split(",");

			for (int i = 0; i < split.length; i++) {

				log.info(split[i].substring(0, split[i].length()));
				String[] s = split[i].split("-");
				valores.add(Double.parseDouble(s[2]));

			}

//			if (max > maiorValor) {
//				maiorValor = max;
//				map.put(string.substring(5,7), maiorValor);
//				key = string.substring(5,7);
//			}

		}

		return Collections.max(valores);

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#getFiles(String)
	 */
	@Override
	public String[] getFiles(String path){

		fileIn = new File(READ_ARQUIVES_DIR);

		String[] dirContents = null;

		if (fileIn.isDirectory()) {

			dirContents = fileIn.list();
			for (int i = 0; i < dirContents.length; i++) {
				log.info(dirContents[i]);
			}

		}

		if (dirContents.length > 0) {

			return dirContents;

		}else{

			dirContents = new String[1];
			dirContents[0] = FILE_NOT_FOUND;

		}

		return dirContents;

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#arquiveTypeVerify(String[])
	 */
	@Override
	public boolean arquiveTypeVerify(String[] files){

		for (int i = 0; i < files.length; i++) {

			if (files[i].endsWith(".dat")) {

				paths.add(READ_ARQUIVES_DIR.concat(separator).concat(files[i]));

			}

		}

		if (!paths.isEmpty()) {

			return true;

		}

		return false;

	}

	/**
	 * @see br.com.nt.projetoteste.interfaces.IDataImportBusiness#arquiveExists(String[])
	 */
	public boolean arquiveExists(String[] files){

		if (!files[0].equals(FILE_NOT_FOUND)) {

			return true;

		}

		return false;

	}


}