package br.com.nt.projetoteste.schedule;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

import br.com.nt.projetoteste.interfaces.IDataImportBusiness;
import br.com.nt.projetoteste.service.DataImportBusiness;
import br.com.nt.projetoteste.util.WarningType;

@Named
@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class ArchiveProcessorSchedule implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Resource
	private SessionContext		 	context;

	@Resource
	private TimerService 			timerService;

	private Logger 					log;

	public int 						countFiles;

	@Inject
	private IDataImportBusiness 	dataImportBusiness;

	@SuppressWarnings(WarningType.STATIC_ACCESS)
	@Schedule(hour = "*", minute = "*", second = "0/20", persistent = false)
	public void importExecutor() {

		String path = null;

		try {
			path = System.getenv(dataImportBusiness.HOMEPATH)
					.concat(dataImportBusiness.separator)
					.concat(dataImportBusiness.READ_ARQUIVES_DIR);
		} catch (Exception e) {
			log.error(e.getMessage());
			return;
		}

		File directory = new File(path);

		if (directory.isDirectory()) {
			if (!directory.canRead()) {
				log.error("Directory has no permission to read.");
				return;
			}

			//executa leitura dos arquivos
			String[] archives = dataImportBusiness.getFiles(path);
			countFiles = archives.length;

			if (dataImportBusiness.arquiveExists(archives)) {
				if (dataImportBusiness.arquiveTypeVerify(archives)) {
					for (int i = 0; i < dataImportBusiness.paths.size(); i++) {
						System.out.println("read arquive "+archives[i]);
						try {
							dataImportBusiness.loadFile(dataImportBusiness.paths.get(i),archives[i]);
						} catch (IOException e) {
							log.error(e.getMessage());
						}
					}
				}else{
					log.error("File is not a .dat type.");
				}
			}else{
				log.error(DataImportBusiness.FILE_NOT_FOUND);
			}
		}else{
			log.error("Its not a directory.");
			return;
		}

	}


}